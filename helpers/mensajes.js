require('colors')

const mostrarMenu = async () => {

  return new Promise((resolve, reject) => {

    //limpiar consola
    console.clear();
    console.log('================================'.yellow);
    console.log('         Picasso Pizzería        '  .red);
    console.log('________________________________'.yellow);
    console.log('          Realizar Pedido          '.red);
    console.log('================================'.yellow);

  console.log(`1. Realizar Pedido`);
  console.log(`2. Lista de pedidos`);
  console.log(`3. Pedidos Confirmados`);
  console.log(`4. Pedidos no Confirmados`);
  console.log(`5. Confirmar Pedidos`);
  console.log(`6. Eliminar Pedido`);
  console.log(`7. Concluir Pedido\n`);
  const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
  });

  readline.question('Seleccione una opcion: ', (opt) => {
    readline.close();
    resolve(opt);
  })

  });

};


  const pausa = () => {

    return new Promise((resolve, reject) => {
      const readline = require('readline').createInterface({
      input: process.stdin,
      output: process.stdout
    });

    readline.question(`\nPresione ${'ENTER'.green} para continuar\n`, () => {

      readline.close();
      resolve()
      
    })
    
    })
  }

module.exports = {
  mostrarMenu, //lo ponemos asi, y el por dentro hará esto --> mostrarMenu: mostrarMenu
  pausa
}