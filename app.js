const { 
  inquirerMenu, 
  pausa, 
  leerInput,
  listadoTareasBorrar,
  confirmar,
  mostrarListadoChecklist
} = require('./helpers/inquirer');
const Tarea = require('./helpers/models/tarea');
const Tareas = require('./helpers/models/tareas');
require('colors');

const { guardarDB, leerDB } = require('./helpers/guardarArchivo')



  const main = async () => {
  // console.log('hola mundo');

  let opt = '';
  const tareas = new Tareas();

  const tareasDB = leerDB()
console.log(tareasDB);

  if(tareasDB){
      tareas.cargarTareasFormArray(tareasDB);
  }

  do{
    opt = await inquirerMenu(); 
    console.log(opt); 

    switch(opt) { 
      case '1':
      console.log('MENU'.yellow);
      console.log('1.- Pizza Margarita'.zebra .green .bold);
      console.log('2.- Pizza cuatro Estaciones'.zebra .green .bold);
      console.log('3.- Pizza de Pepperoni'.zebra .green .bold);
      console.log('4.- Pizza hawaiana'.zebra .white .bold);
      console.log('5.- Pizza Napolitana'.zebra .white .bold);
      console.log('6.- Pizza Siciliana'.zebra .white .bold);
      console.log('7.- Pizza Neoyorquina'.zebra .red .bold);
      console.log('8.- Pizza Fugazza'.zebra .red .bold);
      console.log('9.- Pizza Marinara'.zebra .red .bold);

        const seleccion = await leerInput('Seleccione el número de su Pizza:'.yellow); //en desc se guarda lo del LEERINPUT

      let desc;

        switch (seleccion) {
          case '1': console.log('Pizza Margarita fue añadida a su lista de pedidos'.bold); tareas.crearTarea('Pizza Margarita');
          break;
          case '2': console.log('Pizza cuatro Estaciones fue añadida a su lista de pedidos'.bold); tareas.crearTarea('Pizza cuatro Estaciones');
          break;       
          case '3': console.log('Pizza de Pepperoni fue añadida a su lista de pedidos'.bold); tareas.crearTarea('Pizza de Pepperoni');
          break;   
          case '4': console.log('Pizza hawaiana fue añadida a su lista de pedidos'.bold); tareas.crearTarea('Pizza hawaiana');
          break;  
          case '5': console.log(' Pizza Napolitana fue añadida a su lista de pedidos'.bold); tareas.crearTarea('Pizza Napolitana');
          break;


          default: console.log('ingrese un dato correcto'.red);
          break
      }

      break;
      case '2':
        console.log(tareas.listadoArr);
         tareas.listadoCompleto(); 
      break;
      case '3':
        tareas.listarPendientesCompletadas(true)
      break
      case '4':
        tareas.listarPendientesCompletadas(false)
      break;
      case '5':
        const ids = await mostrarListadoChecklist(tareas.listadoArr);

        tareas.toggleCompletadas(ids);
      break;
      case '6':
        const id = await listadoTareasBorrar(tareas.listadoArr)

        if (id !== '0'){
          const ok = await confirmar('Estas seguro?');
        console.log({ok}); 
        if(ok){
          tareas.borrarTareas(id);
          console.log('Su pedido fue eliminado');
        }
        }
        break;

      default:
        break
    }

    guardarDB(tareas.listadoArr)
    await pausa()
    
  } while(opt !== '7');

};

main();
